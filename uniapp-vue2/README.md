# GoEasy音视频通话（GRTC)Demo运行步骤

## 获取设置appkey
访问http://www.goeasy.io  注册GoEasy账号，创建应用，获得appkey
在main.js里将appkey替换为您自己的common key

## 配置GRTC参数
[访问GRTC文档配置参数](https://docs.goeasy.io/2.x/rtc/qiniu/qiniu-rtc)

## 运行
hbilderX 直接运行即可
