import Vue from 'vue';
import App from './App';
import GoEasy from './lib/goeasy-2.13.13.esm.min.js';
import GRTC from './uni_modules/goeasy-rtc/js_sdk/goeasy-rtc-0.3.6.esm.min.js'

GoEasy.init({
    host:"hangzhou.goeasy.io",//应用所在的区域地址: 【hangzhou.goeasy.io |singapore.goeasy.io】
    appkey:"BC-xxxx",// common key
    modules: ['im'],
    // true表示支持通知栏提醒，false则表示不需要通知栏提醒
    allowNotification: true //仅有效于app,小程序和H5将会被自动忽略
});
GRTC.init(GoEasy);

uni.$GoEasy = GoEasy;
uni.$GRTC = GRTC;

Vue.config.productionTip = false;
App.mpType = 'app';
const app = new Vue({
    ...App
});
app.$mount();